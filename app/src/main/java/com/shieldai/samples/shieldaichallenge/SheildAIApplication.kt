package com.shieldai.samples.shieldaichallenge

import android.app.Application
import timber.log.Timber

class SheildAIApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}