package com.shieldai.samples.shieldaichallenge.backend

import com.google.gson.annotations.SerializedName

data class Episodes(@SerializedName("episodes") val episodes: List<Episode>)