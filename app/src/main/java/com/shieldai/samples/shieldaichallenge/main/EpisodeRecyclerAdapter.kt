package com.shieldai.samples.shieldaichallenge.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.shieldai.samples.shieldaichallenge.R
import com.shieldai.samples.shieldaichallenge.backend.Episode
import com.shieldai.samples.shieldaichallenge.util.appendSpace
import com.shieldai.samples.shieldaichallenge.util.load
import timber.log.Timber
import java.lang.StringBuilder

class EpisodeRecyclerAdapter(
    var episodes: List<Episode> = ArrayList(),
    private val episodeClickListener: ((Episode, Int) -> Unit)? = null
) : RecyclerView.Adapter<EpisodeRecyclerAdapter.EpisodeViewHolder>() {

    private var currentlySelectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false))
    }

    private fun getItemTitle(episode: Episode): String? {
        val season = episode.season
        val ep = episode.number
        val title = episode.name

        val stringBuilder = StringBuilder()
        if(season != null) {
            stringBuilder.append("S").append(season).appendSpace()
        }
        if(ep != null) {
            stringBuilder.append("Ep").append(ep).append(":").appendSpace()
        }
        if(title.isNullOrBlank()) {
            return null
        } else {
            stringBuilder.append(title)
        }
        return stringBuilder.toString()
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        val movie = episodes[position]
        val title = getItemTitle(movie) ?: return // no point in displaying this item if there's no title

        val mediumImageUrl = movie.image?.mediumImageUrl
        if(!mediumImageUrl.isNullOrEmpty()) {
            holder.updateImage(mediumImageUrl)
        }

        holder.updateLabel(title)

        if(currentlySelectedPosition == position) {
            holder.setSelected(true)
        } else {
            holder.setSelected(false)
        }

        holder.itemView.setOnClickListener {
            episodeClickListener?.invoke(movie, position)
            currentlySelectedPosition = position
            notifyDataSetChanged() // trigger a refresh
        }
    }

    override fun getItemCount(): Int {
        return this.episodes.size
    }

    /**
     * Sets the current item as selected
     */
    fun setSelection(position: Int) {
        if(episodes.isEmpty() || position < 0 || position > episodes.size) {
            Timber.w("Unable to set selection for position: $position are episodes empty? ${episodes.isEmpty()}")
            return
        }
        this.currentlySelectedPosition = position
        val episode = episodes[position]
        episodeClickListener?.invoke(episode, position)
        notifyDataSetChanged()
    }

    class EpisodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val imageView: ImageView = itemView.findViewById(R.id.label_thumbnail)
        private val label: TextView = itemView.findViewById(R.id.label_name)

        fun updateImage(url: String) {
            this.imageView.load(url)
        }

        fun updateLabel(label: String) {
            this.label.text = label
        }

        fun setSelected(isSelected: Boolean) {
            if(isSelected) {
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.shield_primary_blue))
            } else {
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.shield_white))
            }
        }
    }
}