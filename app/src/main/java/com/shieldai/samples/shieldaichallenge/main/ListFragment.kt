package com.shieldai.samples.shieldaichallenge.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RawRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shieldai.samples.shieldaichallenge.R
import com.shieldai.samples.shieldaichallenge.backend.Constants
import com.shieldai.samples.shieldaichallenge.util.edit
import java.io.InputStream

class ListFragment : MainFragment(), ListFragmentDelegate {

    private val viewModel = ListFragmentViewModel(this)
    private lateinit var episodeRecycler: RecyclerView
    private lateinit var episodeItemAdapter: EpisodeRecyclerAdapter
    private var lastSelectedPosition = -1 // the last item selected by the user

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.lastSelectedPosition = mainActivity.generalSharedPrefs.getInt(Constants.SELECTED_ITEM_POSITION, -1)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = inflater.inflate(R.layout.list_fragment_layout, container, false)
        this.episodeRecycler = layout.findViewById(R.id.list_fragment_recycler)
        this.episodeItemAdapter = EpisodeRecyclerAdapter()
        this.episodeRecycler.adapter = episodeItemAdapter
        this.episodeRecycler.layoutManager = LinearLayoutManager(context)
        observeEvents()
        return layout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.parseEpisodes()
    }

    private fun updateEpisodes(uiEvent: ListFragmentViewModel.UIEvent.UpdateEpisodes) {

        this.episodeItemAdapter = EpisodeRecyclerAdapter(uiEvent.episodes) { episode, position ->
            mainActivity.generalSharedPrefs.edit {
                putInt(Constants.SELECTED_ITEM_POSITION, position)
                putString(Constants.SELECTED_ITEM_IMAGE_URL, episode.image?.originalImageUrl)
                putString(Constants.SELECTED_ITEM_CONTENT_DESCRIPTION, episode.summaryFormatted)
            }

            this.lastSelectedPosition = position
            mainActivity.onNewMovieClicked(episode.image?.originalImageUrl, episode.summaryFormatted)
        }

        this.episodeRecycler.adapter = episodeItemAdapter
        if(lastSelectedPosition >= 0) {
            this.episodeItemAdapter.setSelection(lastSelectedPosition) // ensure that the last selected position is set when the list is updated.
        }

    }

    private fun observeEvents() {
        viewModel.uiEvents.observeForever { event ->
            when(event) {
                is ListFragmentViewModel.UIEvent.UpdateEpisodes -> updateEpisodes(event)
            }
        }
    }

    override fun getRawResource(@RawRes rawFile: Int): InputStream {
        return resources.openRawResource(rawFile)
    }

}