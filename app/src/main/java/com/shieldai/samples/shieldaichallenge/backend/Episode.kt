package com.shieldai.samples.shieldaichallenge.backend

import androidx.core.text.HtmlCompat
import com.google.gson.annotations.SerializedName

data class Episode(@SerializedName("id") val id: Int?,
                   @SerializedName("url") val url: String?,
                   @SerializedName("name") val name: String?,
                   @SerializedName("season") val season: Int?,
                   @SerializedName("number") val number: Int?,
                   @SerializedName("airdate") val airDate: String?,
                   @SerializedName("airtime") val airTime: String?,
                   @SerializedName("airstamp") val airStamp: String?,
                   @SerializedName("runtime") val runtime: Int?,
                   @SerializedName("image") val image: Image?,
                   @SerializedName("summary") val summary: String?,
                   @SerializedName("_links") val links: Links?) {

    /**
     * Returns summary text with all <> </> tags removed
     */
    val summaryFormatted: String?
        get() {
            if (summary.isNullOrBlank()) return null
            return HtmlCompat.fromHtml(summary, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
        }
}