package com.shieldai.samples.shieldaichallenge.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shieldai.samples.shieldaichallenge.R
import com.shieldai.samples.shieldaichallenge.backend.Episode
import com.shieldai.samples.shieldaichallenge.backend.Episodes
import timber.log.Timber
import java.io.IOException

class ListFragmentViewModel(private val delegate: ListFragmentDelegate) : ViewModel() {

    private val mutableUIEvents: MutableLiveData<UIEvent> = MutableLiveData()

    private val gson = Gson()
    private val episodesJson: String? by lazy {
        try {
            delegate.getRawResource(R.raw.game_of_thrones_episodes).bufferedReader().use { it.readText() }
        } catch (exception: IOException) {
            Timber.e(exception, "Unable to retrieve list of Game of Thrones episodes!")
            null
        }
    }

    private val episodeTypeToken = object : TypeToken<Episodes>() {}.type
    private var episodes: List<Episode> = ArrayList()

    fun parseEpisodes() {
        val episodes: Episodes = gson.fromJson(episodesJson, episodeTypeToken)
        this.episodes = episodes.episodes
        mutableUIEvents.postValue(UIEvent.UpdateEpisodes(this.episodes))
    }

    val uiEvents: LiveData<UIEvent> = mutableUIEvents

    /**
     * Events that trigger updates on UI elements
     */
    sealed class UIEvent {
        class UpdateEpisodes(val episodes: List<Episode>) : UIEvent()
    }
}