package com.shieldai.samples.shieldaichallenge.backend

import com.google.gson.annotations.SerializedName

data class Links(@SerializedName("self") val self: Self?)