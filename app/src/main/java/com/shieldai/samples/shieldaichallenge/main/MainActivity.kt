package com.shieldai.samples.shieldaichallenge.main

import android.os.Bundle
import com.shieldai.samples.shieldaichallenge.R

class MainActivity : BaseActivity() {

    private lateinit var listFragment: ListFragment
    private lateinit var contentFragment: ContentFragment

    override val layoutResId: Int
        get() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listFragment = supportFragmentManager.findFragmentById(R.id.list_fragment) as ListFragment
        contentFragment = supportFragmentManager.findFragmentById(R.id.content_fragment) as ContentFragment
    }

    /**
     * Called when ListFragment reports that a user has clicked on a new movie
     */
    fun onNewMovieClicked(movieImageUrl: String?, movieDescription: String?) {
        contentFragment.updateContent(movieImageUrl, movieDescription)

        if(contentFragment.isVisible) {
            contentFragment.updateContent(movieImageUrl, movieDescription)
        }
    }
}
