package com.shieldai.samples.shieldaichallenge.util

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Global executor pools for the whole application.
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 *
 * For unit testing, note that we can extend this class and inject our own executors for deterministic
 * behavior
 *
 * This is based on the same AppExecutors class defined in the Android architecture repo
 */

const val THREAD_COUNT = 3

class AppExecutors constructor(
    val diskIO: Executor = Executors.newSingleThreadExecutor(),
    val networkIO: ExecutorService = Executors.newFixedThreadPool(THREAD_COUNT),
    val backgroundThreadPool: ExecutorService = Executors.newFixedThreadPool(THREAD_COUNT),
    val mainThread: Executor = MainThreadExecutor()
) {

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }

    private object HOLDER {
        val INSTANCE = AppExecutors()
    }

    companion object {
        private val instance: AppExecutors by lazy { HOLDER.INSTANCE }

        fun execDiskIO(command: Runnable) {
            instance.diskIO.execute(command)
        }

        fun execNetworkIO(command: Runnable) {
            instance.networkIO.execute(command)
        }

        fun execbackgroundThreadPool(command: Runnable) {
            instance.backgroundThreadPool.execute(command)
        }

        fun execMainThread(command: Runnable) {
            instance.mainThread.execute(command)
        }
    }


}