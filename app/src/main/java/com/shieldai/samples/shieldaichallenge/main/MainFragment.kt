package com.shieldai.samples.shieldaichallenge.main

import android.os.Bundle
import androidx.fragment.app.Fragment

abstract class MainFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        require(context is MainActivity) { "Fragments extending MainFragment must only be used with MainActivity" }
    }

    protected val mainActivity: MainActivity by lazy { context as MainActivity }
}