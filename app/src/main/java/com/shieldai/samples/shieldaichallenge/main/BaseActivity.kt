package com.shieldai.samples.shieldaichallenge.main

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shieldai.samples.shieldaichallenge.backend.Constants

abstract class BaseActivity : AppCompatActivity() {

    /**
     * The layout to be used for this activity
     */
    abstract val layoutResId: Int

    val generalSharedPrefs: SharedPreferences
        get() = applicationContext.getSharedPreferences(Constants.SHARED_PREFS_TAG, Context.MODE_PRIVATE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
    }
}