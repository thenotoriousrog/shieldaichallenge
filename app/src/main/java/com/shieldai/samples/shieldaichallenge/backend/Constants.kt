package com.shieldai.samples.shieldaichallenge.backend

object Constants {
    const val SHARED_PREFS_TAG = "sharedPrefsTag"
    const val SELECTED_ITEM_POSITION = "selectedItemPosition"
    const val SELECTED_ITEM_IMAGE_URL = "selectedItemImageUrl"
    const val SELECTED_ITEM_CONTENT_DESCRIPTION = "selectedItemContentDescription"
}