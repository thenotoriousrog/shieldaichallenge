package com.shieldai.samples.shieldaichallenge.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.android.material.textview.MaterialTextView
import com.shieldai.samples.shieldaichallenge.R
import com.shieldai.samples.shieldaichallenge.backend.Constants
import com.shieldai.samples.shieldaichallenge.util.load
import timber.log.Timber

class ContentFragment : MainFragment() {

    private lateinit var imageView: ImageView
    private lateinit var episodeSummary: MaterialTextView

    /**
     * Updates the fragment if and only if it's still visible to the user.
     * Recommended to check the fragment's visibility before calling
     */
    fun updateContent(movieImageUrl: String?, movieDescription: String?) {
        if(isVisible) {
            this.imageView.load(movieImageUrl)
            this.episodeSummary.text = movieDescription
        } else {
            Timber.w("Content was not updated successfully! Are you checking the fragment's visibility?")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = inflater.inflate(R.layout.item_content_fragment_layout, container, false)

        this.imageView = layout.findViewById(R.id.movie_image)
        this.episodeSummary = layout.findViewById(R.id.episode_summary)

        val url = mainActivity.generalSharedPrefs.getString(Constants.SELECTED_ITEM_IMAGE_URL, null)
        val summary = mainActivity.generalSharedPrefs.getString(Constants.SELECTED_ITEM_CONTENT_DESCRIPTION, null)
        this.imageView.load(url)
        this.episodeSummary.text = summary

        return layout
    }
}