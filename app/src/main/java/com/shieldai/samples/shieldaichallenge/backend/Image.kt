package com.shieldai.samples.shieldaichallenge.backend

import com.google.gson.annotations.SerializedName

data class Image(@SerializedName("medium") val mediumImageUrl: String?,
                 @SerializedName("original") val originalImageUrl: String?
)