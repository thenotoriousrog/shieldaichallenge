package com.shieldai.samples.shieldaichallenge.main

import androidx.annotation.RawRes
import java.io.InputStream

/**
 * Helps separate certain items available to fragments i.e. contexts away from the viewmodel
 */
interface ListFragmentDelegate {

    /**
     * Retrieves a raw resource
     * @param rawFile - the file to retrieve
     * @return - the input stream of the rawFile requested
     */
    fun getRawResource(@RawRes rawFile: Int): InputStream
}