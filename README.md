# Shield AI Coding Challange

## Architecture 
I chose the to use MVVM for the architecture for this project. The architecture design pattern wasn't stated in the challenge write-up itself, but based off my conversation with a few of the Shield AI interviewers it definitely seemed that MVVM was what they like and what Shield AI likely uses. I typically prefer MVP, but have no problem doing it in MVVM. 

## Libraries Used
* Timber - for logging, warnings, and error handling
* Glide - I wanted an easy way to display images in the recyclerview. Glide is an excellent framework for that
* Gson - easier json formatting

## Reasonings
* I chose to use two fragments to accomplish this task because I found it to be easier. I wanted the fragments to be in charge of their UI elements and control the logic that handled user actions. I used the MainActivity more so as a liason between the two fragments which I named ListFragment, which displayed the Game Of Thrones episodes, and the ContentFragment, which showed the episode image and summary. 
* I also made use of Kotlin extensions. I did so because Kotlin Extensions are awesome and I loved adding additional methods which can help make code more readable and useful. 
* I used SharedPreferences to store the user information because it was the easiest choice. For something like this I didn't feel it was necessary to have a database.
* I made abstract classes for the Activity and the Fragments because I wanted to show my intentions on how this could be scaled up. In production code, it's very common to have a BaseActivity for shared logic and I wanted to reflect that I was thinking about those things in case this project ever needed to be scaled up (theoretically)
* I didn't make use of the Executors at all. The reason for that was simply because the libraries I used didn't really need it. The libraries were able to handle the work I passed to them without the need for an executor. I would likely have used them as the work ramped up.

## What I'd do differently
* I would have added Unit Tests. I was running low on-time and didn't want to exceed the 4 hour mark too much.
* I missed the email that contain the instructions on how what was expected in the app when I built it. I built the app solely on watching the gifs provided to me. After finishing the work I looked through my email and saw that there were instructions. Thankfully, I was fairly close to what was expected already so I didn't really need to do much. I would have read the instructions before starting but hey, at least it worked out. So I built the app in on part with the images instead of the first part. 

